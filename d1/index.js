
// CRUD Operations
	/*
		- CRUD is an acronym for: Create, Read, Update, and Delete
		- Create: The create function allows users to create a new record in the database
		- Read: The read function is similar to search function. It allows users to search and retrieve specific records.
		- Update: The update function is used to modify existing records that are on our database.
		- Delete: The delete function allows users to remove records from the database that is no longer needed.
	*/

// CREATE: INSERT Documents
	/*
		- The Mongo shell uses JavaScript for its syntax
		- MongoDB deals with objects as it's structures for documents.
		- We can create documents by providing objects into our methods.
		- JavaScript Syntax:
			- object.object.method({object})
	*/

// insertOne()
	/*
		Syntax:
			db.collectionName.insertOne({object});
	*/

	db.users.insertOne({
		firstName: "Jane",
		lastName: "Doe",
		age: 21,
		contact: {
			phone: "87654321",
			email: "janedoe@mail.com"
		}
	});

// insertMany()
	/*
		Syntax:
			db.collectionName.insertMany([{objectA}, {ObjectB}]);
	*/

	db.users.insertMany([
		{
			firstName: "Stephen",
			lastName: "Hawking",
			age: 76,
			contact: {
				phone: "87654321",
				email: "stephenhawking@mail.com"
			},
			course: ["Python", "React", "PHP"],
			department: "none"
		},
		{
			firstName: "Neil",
			lastName: "Armstrong",
			age: 82,
			contact: {
				phone: "87654321",
				email: "neilarmstrong@mail.com"
			},
			course: ["React", "Laravel", "Sass"],
		}
	]);


// READ: FIND/RETRIEVE Documents
	/*
		- The documents will be returned based on their order of storage in the collection
	*/

// find()
	// Find all Documents
		/*
			Syntax:
				db.collectionName.find();
		*/
		db.users.find();

	// Find using SINGLE parameter
		/*
			Syntax:
				db.collectionName.find({ field: value});
		*/
		db.users.find({firstName: "Stephen"});

	// Find using MULTIPLE Parameter
		/*
			Syntax:
				db.collectionName.find({ fieldA: valueA, fieldB: valueB});
		*/
		db.users.find({lastName: "Armstrong", age: 82});

	// Find + Pretty Method
		/*
			- The 'Pretty' methods allows us to be able to view the documents returned by our terminal in a 'prettier' format.

			Syntax:
				db.collectionName.find({field:value}).pretty();
		*/
		db.users.find({lastName: "Armstrong", age: 82}).pretty();


// UPDATE: EDIT a Document
	
// updateOne()
	// Updating a SINGLE document
	/*
		Syntax:
			db.collectionName.updateOne({criteria}, {$set: {field:value}});
	*/

	// For example, let us create a document that we will then update
		// 1. Insert initial document
			db.users.insertOne({
				firstName: "Test",
				lastName: "Test",
				contact: {
					phone: "0000000",
					email: "test@mail.com"
				},
				course: [],
				department: "none"
			});

		// 2. Update the document
			db.users.updateOne(
				{firstName: "Test"},
				{
					$set: {
						firstName: "Bill",
						lastName: "Gates",
						age: 65,
						contact: {
							phone: "87654321",
							email: "bill@mail.com"
						},
						course: ["PHP", "Laravel", "HTML"],
						department: "Operations",
						status: "active"
					}
				}

			);
		// Return the document
		db.users.find({firstName: "Bill"}).pretty();

// updateMany()
	// Updates MULTIPLE documents
	/*
		Syntax:
			db.collectionName.updateMany({criteria}, {$set: {field:value}});
	*/
	db.users.updateMany(
		{department: "none"},
		{
			$set: {department: "HR"}
		}
	);
	// return
	db.users.find().pretty();

// replaceOne()
	/*
		- Replace one replaces the whole documents
		- If updateOne updates specific fields, replacesOne replaces the whole document
		- If updateOne updates parts, replaceOne replaces the whole document
	*/
	db.users.replaceOne(
		{firstName: "Bill"},
		{
			firstName: "Bill",
			lastName: " Gates",
			age:65,
			contact: {
				phone: "12345678",
				email: "bill@rocketmail.com"
			},
			course: ["PHP", "Laravel", "HTML"],
			department: "Operations"
		}	
	);
	db.users.find({firstName: "Bill"});


// DELETE: DELETING Documents
	// For example, let us create a document that we will delete
	// It is good to practice soft deleteion or archiving of our documents instead of deleting them or removing them in the system.

		db.users.insertOne({
			firstName: "test"
		});

// deleteOne()
	// Deleting a SINGLE document
		/*
			Syntax:
				db.collectionName.deleteOne({criteria});
		*/

		db.users.deleteOne({
			firstName: "test"
		});
		db.users.find({firstName: "test"});

// deleteMany()
	// delete MANY documents
		/*
			Syntax:
				db.collectionName.deleteMany({criteria});
		*/

		db.users.deleteMany({
			firstName: "Bill"
		});
		db.users.find({firstName: "Bill"});


// deleteAll()
	// Delete ALL documents
		/*
			Syntax:
				db.collectionName.deleteMany({});
		*/


// ADVANCED QUERIES
	/*
		- Retrieveing data with complex data structrues is also a good skill for any developer to have
		- Real world examples of data can be as complex as having two or more layers of nested objects
		- Learning to query these kinds of data is also essential to ensure that we are able to retrieve any information that we would need in our application
	*/

// Query an Embedded document
	// An embedded document are those types of documents that contains a document inside a document.

	db.users.find({
		contact: {
			phone: "12345678",
			email: "bill@rocketmail.com"
		}
	});	

// Query on Nested Field

	db.users.find({
		"contact.email": "janedoe@mail.com"
	});

// Querying an Array with Exact Elements
	
	db.users.find({course: ["CSS", "JavaScript", "Python"]});
	

// Querying an Array without regard to order

	db.users.find({courses: {$all: ["React", "Python"]}});

// Querying an Embedded Array

	db.users.insertOne({
		namearr: [
			{
				nameA: "Juan",
			},
			{
				nameB: "Tamad"
			}
		]
	});
	db.users.find({
		namearr:
		{
			nameA: "Juan"
		}
	});